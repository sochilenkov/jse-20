package ru.t1.sochilenkov.tm.api.service;

import ru.t1.sochilenkov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
