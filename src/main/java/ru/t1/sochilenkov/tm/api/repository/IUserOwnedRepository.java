package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M add(String userId, M model);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}
