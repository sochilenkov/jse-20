package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null || task.getUserId() == null) continue;
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

}
