package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.api.service.IAuthService;
import ru.t1.sochilenkov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout from current user.";

    public static final String NAME = "logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final IAuthService authService = getAuthService();
        authService.logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
